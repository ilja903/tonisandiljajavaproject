package controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import model.Game;

import org.junit.Test;

public class AutomaticGameDispatcherTest {

	@Test
	public void testGetTeams() {
		assertTrue("there must be more than 0 movies after initializing", 0 < AutomaticGameDispatcher.INSTANCE.getTeams().size());
	}

	@Test
	public void testGenerateGame() {
		AutomaticGameDispatcher.INSTANCE.generateGame();
		Game currentGame = AutomaticGameDispatcher.INSTANCE.getCurrentGame();
		assertNotNull(currentGame);
		assertFalse("1 team cannot play for both sides",currentGame.getDefendingTeam()==currentGame.getAttackingTeam());
	}
	

	
}
