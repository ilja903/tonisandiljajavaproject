package controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import controller.gamelogic.BetMachineTest;
import controller.gamelogic.GamePlayerTest;
import controller.gamelogic.GameSabotagerTest;
import controller.gamelogic.TeamInvestorTest;

@RunWith(Suite.class)
@SuiteClasses({ BetMachineTest.class, AutomaticGameDispatcherTest.class, GamePlayerTest.class,
		TeamInvestorTest.class, GameSabotagerTest.class, BankTest.class})
public class AllTests {

}
