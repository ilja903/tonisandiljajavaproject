package controller.gamelogic;

import static org.junit.Assert.*;

import model.Game;
import model.GameState;
import model.Money;
import model.Team;
import model.User;

import org.junit.Test;

import controller.applicationlogic.TeamInvestor;

public class TeamInvestorTest {

	@Test
	public void testInvestIntoTeam() {
		Team barcelona = new Team("Barcelona");
		User ilja = new User("ilja");
		assertFalse("user dont have so much money",TeamInvestor.INSTANCE.investIntoTeam(ilja, barcelona, new Money(10000000)));
		assertTrue("Ok",TeamInvestor.INSTANCE.investIntoTeam(ilja, barcelona, new Money(1000000)));
		assertEquals("barcelona will gain 100 levels", 101, barcelona.getLevel());
		assertEquals("user dont have money", 0, ilja.getMoney().toInt());
	}

}
