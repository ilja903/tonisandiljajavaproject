package controller.gamelogic;

import static org.junit.Assert.*;

import java.util.List;

import model.Game;
import model.Money;
import model.Sabotage;
import model.Team;
import model.User;

import org.junit.Test;

import controller.applicationlogic.GamePlayer;
import controller.applicationlogic.GameSabotager;
import controller.applicationlogic.gameplayinglogic.SabotageGamePlayingLogic;

public class GameSabotagerTest {

	@Test
	public void testCollectSabotagesInGivenGame() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		Game revenge = new Game("Revenge ", barcelona, madrid);
		User ilja = new User("ilja");
		GameSabotager.INSTANCE.sabotageTeamInGame(ilja, madrid, epicGame, new Money(10000));
		GameSabotager.INSTANCE.sabotageTeamInGame(ilja, madrid, epicGame, new Money(10000));
		List<Sabotage> sabotagesInEpicGame = GameSabotager.INSTANCE.collectSabotagesInGivenGame(epicGame);
		assertEquals("epicagame has 2 sabotages", 2, sabotagesInEpicGame.size());
		List<Sabotage> sabotagesInRevenge = GameSabotager.INSTANCE.collectSabotagesInGivenGame(revenge);
		assertEquals("epicagame has no sabotages", 0, sabotagesInRevenge.size());
	}

	@Test
	public void testSabotagingInGivenGame() {
		for (int i = 0; i < 1000; i++) {
			Team barcelona = new Team("Barcelona");
			Team madrid = new Team("Madrid");
			Game epicGame = new Game("Football world cup", barcelona, madrid);
			User ilja = new User("ilja");
			GameSabotager.INSTANCE.sabotageTeamInGame(ilja, madrid, epicGame, new Money(100000));
			GamePlayer.INSTANCE.playGame(epicGame);
			assertEquals("madrid will loose, barcelona will win", epicGame.getWinningTeam(), barcelona);
		}
	}

}
