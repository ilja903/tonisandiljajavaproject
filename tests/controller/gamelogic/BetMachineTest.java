package controller.gamelogic;

import static org.junit.Assert.*;

import model.Game;
import model.GameState;
import model.Money;
import model.Team;
import model.User;

import org.junit.Test;

import controller.applicationlogic.BetMachine;
import controller.applicationlogic.GamePlayer;

public class BetMachineTest {

	@Test
	public void testPutMoneyOn() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		User ilja = new User("ilja");
		assertEquals("testing his account", 1000000, ilja.getMoney().toInt());
		BetMachine.INSTANCE.putMoneyOn(epicGame, madrid, new Money(200), ilja);
		assertEquals("testing his account", 999800, ilja.getMoney().toInt());
		GamePlayer.INSTANCE.playGame(epicGame);

		// added BetMachine bet cut logic, so new asserts were needed
		if (epicGame.getWinningTeam().equals(madrid)) {
			assertTrue("testing his account for winning", ilja.getMoney().toInt() > 1000000);
		} else {
			assertTrue("testing his account for losing", ilja.getMoney().toInt() < 1000000);
		}
		assertEquals("game must be finished", GameState.FINISHED, epicGame.getGameState());
	}

	@Test
	public void testPutMoneyMoreThanPlayerHas() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		User ilja = new User("ilja");
		assertEquals("testing his account", 1000000, ilja.getMoney().toInt());
		BetMachine.INSTANCE.putMoneyOn(epicGame, madrid, new Money(1000300), ilja);
		assertEquals("testing his account", 1000000, ilja.getMoney().toInt());
		GamePlayer.INSTANCE.playGame(epicGame);
		assertEquals("testing his account", 1000000, ilja.getMoney().toInt());
		assertEquals("game must be finished", GameState.FINISHED, epicGame.getGameState());
	}


	
//	@Test
//	public void testLoanThrowUserOut() {
//		User mauno = new User("mauno");
//		BetMachine.INSTANCE.takeLoan(new Money(100), mauno);
//		mauno.getMoney().set(0);
//		Team barcelona = new Team("Barcelona");
//		Team madrid = new Team("Madrid");
//		Game epicGame;
//		for (int i = 0; i < 4; i++) {
//			epicGame = new Game("Football world cup", barcelona, madrid);
//			GamePlayer.INSTANCE.playGame(epicGame);
//		}
//		assertTrue("user should not be listed anymore", !User.getUsers().contains(mauno));
//	}

}
