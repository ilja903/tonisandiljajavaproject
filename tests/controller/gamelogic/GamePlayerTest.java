package controller.gamelogic;

import static org.junit.Assert.*;

import model.Game;
import model.GameState;
import model.Team;
import model.User;

import org.junit.Test;

import controller.applicationlogic.GamePlayer;

import exceptions.NoWinningTeamException;

public class GamePlayerTest {

	@Test
	public void testPlayGame() throws NoWinningTeamException {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		GamePlayer.INSTANCE.playGame(epicGame);
		assertEquals("game must be finished", GameState.FINISHED, epicGame.getGameState());
		int sumOfLevels = barcelona.getLevel() + madrid.getLevel();
		assertEquals("each team had 1 level,one of them won and gained additional level", 3, sumOfLevels);
		if (epicGame.getWinningTeam().equals(barcelona)) {
			assertEquals("if won barcelona then it has gained level", 2, barcelona.getLevel());
		}else if(epicGame.getWinningTeam().equals(madrid)){
			assertEquals("if won madrid then it has gained level", 2, madrid.getLevel());			
		}
	}
	
	
	public void testPlayGameWithMoney() throws NoWinningTeamException {
		User ilja = new User("ilja");
		User tonis = new User("tonis");
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		GamePlayer.INSTANCE.playGame(epicGame);
	}

}
