package controller;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import model.Game;
import model.Money;
import model.Team;
import model.User;

import org.junit.Test;

import controller.applicationlogic.GamePlayer;
import controller.bank.Bank;


public class BankTest {

	@Test
	public void testTakeLoan() {
		User tonis = new User("tonis");
		Bank.INSTANCE.takeLoan(new Money(200), tonis);
		assertEquals("users money must have increased by 200", 1000200, tonis.getMoney().toInt());
	}

	@Test
	public void testTakeLoanThatExceedsBank() {
		User tonis = new User("tonis");
		int bankAmount = Bank.INSTANCE.getBank().toInt();
		boolean resultOfLoan = Bank.INSTANCE.takeLoan(new Money(bankAmount + 100), tonis);
		assertFalse("loan should have failed", resultOfLoan);
		assertEquals("user should have initial amount of money", 1000000, tonis.getMoney().toInt());
	}
	
	@Test
	public void testLoanPayback() {
		User mauno = new User("mauno");
		Bank.INSTANCE.takeLoan(new Money(200), mauno);
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame;
		for (int i = 0; i < 4; i++) {
			epicGame = new Game("Football world cup", barcelona, madrid);
			GamePlayer.INSTANCE.playGame(epicGame);
		}
		assertTrue("user should have payed twice the loan amount", mauno.getMoney().toInt() == 999800);
	}
}
