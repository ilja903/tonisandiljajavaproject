package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {

	@Test
	public void testUser() {
		User user = new User("ilja");
		assertEquals("testing a name", "ilja" ,user.getName());
		assertEquals("testing his account", 1000000, user.getMoney().toInt());
	}

	@Test
	public void testGetUserByName() {
		User ilja = new User("ilja");
		User ilja2 = new User("ilja2");
		User i2 = User.getUserByName("ilja2");
		assertSame("same user",ilja2,i2);
	}
}
