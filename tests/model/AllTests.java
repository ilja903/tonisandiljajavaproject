package model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MoneyTest.class, UserTest.class, TeamTest.class, GameTest.class })
public class AllTests {

}
