package model;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.applicationlogic.BetMachine;

public class MoneyTest {



	@Test
	public void testGetAmount() {
		Money money = new Money(0);
		assertNotNull("at First object cannot be null", money.toInt());
		assertEquals("Money object must be 0", 0, money.toInt());
	}

	@Test
	public void testSetAmount() {
		Money money = new Money(0);
		money.set(200);
		assertEquals("there must be must be 200 dollarz", 200, money.toInt());
	}
	
	@Test
	public void testAddAmount() {
		Money money = new Money(0);
		money.set(200);
		money.add(1);
		assertEquals("there must be must be 201 dollar", 201, money.toInt());
	}

}
