package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TeamTest {
	@Before
	public void init() {
		Team.getTeams().clear();
	}

	@Test
	public void testGetTeamByName() {
		Team joj = new Team("joj");
		new Team("loj");
		Team madrid = new Team("madrid");
		assertSame("must be the same", joj, Team.getTeamByName("joj"));
		assertSame("must be the same", madrid, Team.getTeamByName("madrid"));
	}

	@Test
	public void testGetTeams() {
		Team joj = new Team("joj");
		new Team("loj");
		Team madrid = new Team("madrid");
		assertEquals("must be 3 elemnts inside", 3, Team.getTeams().size());
	}

	@Test
	public void testIncrementLevel() {
		Team team = new Team("Torros");
		assertEquals("team starts with level 1", 1, team.getLevel());
		team.incrementLevel();
		team.incrementLevel();
		assertEquals("team now has level 3", 3, team.getLevel());
	}

	@Test
	public void testGetLevel() {
		Team team = new Team("Torros");
		assertEquals("team starts with level 1", 1, team.getLevel());
	}

	@Test
	public void testIncrementLevelNTimes() {
		Team team = new Team("Torros");
		assertEquals("team starts with level 1", 1, team.getLevel());
		team.incrementLevelNtimes(5);
		assertEquals("team now has level 6", 6, team.getLevel());
	}

}
