package model;

import static org.junit.Assert.*;

import org.junit.Test;

import exceptions.NoWinningTeamException;
import exceptions.TeamHasAlreadyWonException;

public class GameTest {

	@Test
	public void testGame() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		System.out.println(epicGame);
	}

	@Test
	public void testGetAttackingTeam() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		assertEquals("in constructor barcelona is attacking team", barcelona, epicGame.getAttackingTeam());
	}

	@Test
	public void testGetDefendingTeam() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		assertEquals("in constructor madrid is defending team", madrid, epicGame.getDefendingTeam());
	}

	@Test
	public void testGetWinningTeam() {
		Team barcelona = new Team("Barcelona");
		Team madrid = new Team("Madrid");
		Game epicGame = new Game("Football world cup", barcelona, madrid);
		try {
			epicGame.setWinningTeam(madrid);
			assertEquals("madrid should be the winning team", madrid, epicGame.getWinningTeam());

		} catch (TeamHasAlreadyWonException e) {
			System.out.println("team has already won");
			fail();
		}
	}
}
