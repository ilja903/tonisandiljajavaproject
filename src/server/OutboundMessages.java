package server;


import java.util.LinkedList;

/**
 * Contains all messages in a linked list that need to be distributed.
 *
 */
public class OutboundMessages {
	private LinkedList<String> messages = new LinkedList<String>();

	/**
	 * Adds a new message to the list of messages.
	 * @param s - the content of the message
	 */
	public synchronized void addMessage(String s) {
		messages.add(s);
		System.out.println(s);
		this.notifyAll();
	}

	/**
	 * Tries to fetch the first message from the 
	 * messages. If there are no messages, it waits 
	 * until there are, and then gets the first 
	 * message in the list.
	 * @return the first message available
	 */
	public synchronized String getMessage() { 
		try {
			while (messages.isEmpty())
				this.wait(); 
		} catch (InterruptedException e) {}

		String s = messages.getFirst();
		messages.removeFirst();
		return s;
	}
}