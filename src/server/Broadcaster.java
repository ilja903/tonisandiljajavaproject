package server;



import java.util.Iterator;

/**
 * A thread that takes Messages from OutboundMessages and 
 * distributes them to all active sessions. If a ClientSession 
 * if found to be not alive during distribution, it is removed
 * from the active sessions list.
 *
 */
class Broadcaster extends Thread {
	private ActiveSessions activeSessions;
	private OutboundMessages outQueue;

	Broadcaster(ActiveSessions aa, OutboundMessages o) {
		activeSessions = aa;
		outQueue = o;
		start();
	}

	public void run() {
		while (true) {
			String s = outQueue.getMessage();
			
			synchronized (activeSessions) {
				Iterator<ClientSession> active = activeSessions.iterator();
				
				while (active.hasNext()) {
					ClientSession cli = active.next();
					
					if (!cli.isAlive()) {
						active.remove();
					} else { 
						cli.sendMessage(s);
					}
				}
			}
		}
		
	}
}