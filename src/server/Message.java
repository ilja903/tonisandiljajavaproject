package server;


public class Message {
	
	private String contents;

	public Message(String contents) {
		super();
		this.contents = contents;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	@Override
	public String toString() {
		return "Message [contents=" + contents + "]";
	}

}
