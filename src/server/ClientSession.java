package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import model.User;

import controller.Controller;

/**
 * A thread that corresponds to one client.
 * 
 */
public class ClientSession extends Thread {
	private Socket socket;
	public static OutboundMessages messageToEveryone;
	private ActiveSessions activeSessions;
	private BufferedReader netIn;
	private PrintWriter personalClientOutput;
	private User clientUser;
	
	public PrintWriter getPersonalClientOutput() {
		return personalClientOutput;
	}

	public OutboundMessages getOutboundMessages() {
		return messageToEveryone;
	}


	private volatile boolean executing = true;

	public ClientSession(Socket s, OutboundMessages out, ActiveSessions as) throws IOException {
		socket = s;
		messageToEveryone = out;
		activeSessions = as;
		netIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		personalClientOutput = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
				socket.getOutputStream())), true);
		System.out.println("ClientSession " + this + " stardib...");
		start();
	}

	public void run() {
		try {
			String name;
			personalClientOutput.println("Type in your username:");
			while (true) {
				name = netIn.readLine();
				if (name.length() < 3) {
					personalClientOutput.println("Name too short, choose something else");
					continue;
				}
				super.setName(name);
				break;
			}

			User user = authorizeUser(name);

			activeSessions.addSession(this);

			String youTypedIn = user + " have connected!";
			messageToEveryone.addMessage(youTypedIn);

			while (executing) {
				youTypedIn = netIn.readLine();
				if (youTypedIn != null) {
					Controller.INSTANCE.manage(messageToEveryone, user, youTypedIn,
							personalClientOutput);
				} else {
					messageToEveryone.addMessage(user.getName() + " has left");
					User.getUsers().remove(user);
					break;
				}
			}

		} catch (IOException e) {
		}
	}

	private User authorizeUser(String name) {
		User user;
		if (User.getUserByName(name) == null) {
			user = new User(name);
			setClientUser(user);
		} else {
			user = User.getUserByName(name);
		}
		return user;
	}

	public void sendMessage(String msg) {
		try {
			if (!socket.isClosed()) {
				personalClientOutput.println(msg);
			} else {
				throw new IOException();
			}
		} catch (IOException eee) {
			messageToEveryone.addMessage(getName() + " - avarii...");
			try {
				socket.close();
			} catch (IOException ee) {
			}
		}
	}

	public void stopExecuting() {
		this.executing = false;
	}

	public User getClientUser() {
		return clientUser;
	}

	public void setClientUser(User clientUser) {
		this.clientUser = clientUser;
	}
}