package server;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * List of all currently active sessions.
 *
 */
public class ActiveSessions {
	private static Collection<ClientSession> sessionList 
			= new ArrayList<ClientSession>();

	public synchronized void addSession(ClientSession s) {
		sessionList.add(s);
		System.out.println("New client has arrived: " + s);
	}

	public static Iterator<ClientSession> iterator() {
		return sessionList.iterator();
	}
}