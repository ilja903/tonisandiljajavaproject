package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import controller.AutomaticGameDispatcher;


/**
 * The server that accepts sockets on a certain port (8888).
 *
 */
public class Server {
	private static final int PORT = 8888;

	public static void main(String[] args) throws IOException {
		
		AutomaticGameDispatcher.INSTANCE.executeAutomaticDispacher();
		
		ActiveSessions activeSessions = new ActiveSessions();
		OutboundMessages outQueue = new OutboundMessages();

		System.out.println("Server startis...");
		
		new Broadcaster(activeSessions, outQueue);
		
		try(ServerSocket serv = new ServerSocket(PORT)) {
			while (true) {
				Socket sock = serv.accept();
				try {
					new ClientSession(sock, outQueue, activeSessions);
				} catch (IOException e) {
					System.out.println("Socketi loomise avarii");
					sock.close();
				}
			} 

		}
	}
}