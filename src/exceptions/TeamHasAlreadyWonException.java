package exceptions;

public class TeamHasAlreadyWonException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TeamHasAlreadyWonException(String errorMessage) {
		super(errorMessage);
	}

}
