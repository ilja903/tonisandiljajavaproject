package exceptions;

public class NoWinningTeamException extends GameException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoWinningTeamException(String errorMessage) {
		super(errorMessage);
	}
	

}
