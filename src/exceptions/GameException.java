package exceptions;

public class GameException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected GameException(String errorMessage) {
		super(errorMessage);
	}
}
