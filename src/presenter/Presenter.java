package presenter;

import java.util.Iterator;

import model.Game;
import model.Money;
import model.User;
import util.LoggerAndNetWriter;

/**
 * @author ilja Sort of View in MVC, but more all-in-one approach (only one
 *         class render all views). However, we may divide it.
 */
public enum Presenter {
	INSTANCE;
	public void showUsers(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput,
			Iterator<User> iterator) {
		userConsoleAndGameLogOutput.println("|| LIST OF USERS ||");
		while (iterator.hasNext()) {
			User user = iterator.next();
			userConsoleAndGameLogOutput.println(user);
		}
	}

	public void showUserMoney(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput, Money money) {
		userConsoleAndGameLogOutput.println("|| YOU HAVE ||");
		userConsoleAndGameLogOutput.println(money);
	}

	public void showCurrentGame(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput, Game game) {
		userConsoleAndGameLogOutput.println("|| GAMES RIGHT NOW ||");
		userConsoleAndGameLogOutput.println(game);
	}

	public void createBet(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput,
			boolean isMoneyPut) {
		if (isMoneyPut) {
			userConsoleAndGameLogOutput.println("|| YOUR BET ACCEPTED ||");
		} else {
			userConsoleAndGameLogOutput.println("|| COULD NOT PLACE BET ||");
		}
	}

	public void takeLoan(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput,
			boolean isLoanTaken) {
		if (isLoanTaken) {
			userConsoleAndGameLogOutput.println("|| YOUR LOAN ACCEPTED ||");
		} else {
			userConsoleAndGameLogOutput.println("|| COULD NOT TAKE LOAN ||");
		}
	}

	public void showInvestmentMessage(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput,
			boolean isInvested) {
		if (isInvested) {
			userConsoleAndGameLogOutput.println("|| YOUR INVESTMENT ACCEPTED ||");
		} else {
			userConsoleAndGameLogOutput.println("|| YOUR INVESTMENT IS NOT ACCEPTED ||");
		}

	}

	public void showHelp(LoggerAndNetWriter userConsoleAndGameLogOutput) {
		userConsoleAndGameLogOutput
				.printf("Commands:%n%n"
						+ "-users %n Show all active users%n"
						+ "-games %n Show current game%n"
						+ "-money %n Show the amount of money you have%n"
						+ "-bet [TEAM] [AMOUNT] %n Bet the chosen amount on the chosen team%n"
						+ "-invest [TEAM] [AMOUNT] %n Invest the chosen amount in the team, increasing its odds to win (minumum step is 10000)%n"
						+ "-sabotage [TEAM] [AMOUNT] %n Sabotage the chosen team according to the money, decreasing its odds to win%n"
						+ "-loan [AMOUNT] %n Loan the chosen amount. Cannot exceed the banks available funds%n"
						+ "-buy [ITEM] %n Buys specified item (cheapvuvu, fancyvuvu, drums)%n"
						+ "-use %n Uses the item you are currently holding%n"
						+ "-quit %n Leave this hell-hole");
		userConsoleAndGameLogOutput.println();
	}

	public void showSabotageMessage(User currentUser, LoggerAndNetWriter userConsoleAndGameLogOutput,
			boolean isSabotaged) {
		if (isSabotaged) {
			userConsoleAndGameLogOutput.println("|| YOUR SABOTAGE SUCCEDED ||");
		} else {
			userConsoleAndGameLogOutput.println("|| YOUR SABOTAGE WAS NOT SUCCESFUL ||");
		}
		
	}

	public void buyItem(User currentUser,
			LoggerAndNetWriter userConsoleAndGameLogOutput, boolean isItemBought) {
		if (isItemBought) {
			userConsoleAndGameLogOutput.println("|| YOUR PURCHASE SUCCEDED ||");
		} else {
			userConsoleAndGameLogOutput.println("|| YOUR PURCHASE WAS NOT SUCCESFUL ||");
		}
	}

}
