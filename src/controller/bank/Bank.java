package controller.bank;

import java.util.Iterator;

import model.Loan;
import model.Money;
import model.User;
import server.ActiveSessions;
import server.ClientSession;
import util.ErrorInformer;

public enum Bank {
	INSTANCE;
	private int initialBankAmount = 1000000;
	private Money bank = new Money(initialBankAmount);
	private int coefficentOfLoan = 2;

	public boolean takeLoan(Money money, User loaner) {
		if (hasActiveLoan(loaner)) {
			ErrorInformer.INSTANCE.inform("You already have an active loan", loaner);
			return false;
		}

		if (bank.toInt() < money.toInt()) {
			ErrorInformer.INSTANCE.inform("The bank does not have enough money to loan", loaner);
			return false;
		}

		bank.subtract(money.toInt());
		loaner.getMoney().add(money.toInt());
		return Loan.getLoans().add(new Loan(money, loaner));
	}

	public void manageLoans() {
		Iterator<Loan> iterator = Loan.getLoans().iterator();
		while (iterator.hasNext()) {
			Loan loan = iterator.next();
			loan.decreasePayBackCounter();
			if (loan.getLoaner() != null && loan.getPayBackCounter() < 0) {
				User loaner = loan.getLoaner();
				if (loan.getMoney().toInt() * coefficentOfLoan > loaner.getMoney().toInt()) {
					Loan.getLoans().remove(loan);
					BankCollector.INSTANCE.punishLoaner(loaner);
				} else {
					int loanAmount = loan.getMoney().toInt() * coefficentOfLoan;
					loaner.getMoney().subtract(loanAmount);
					Bank.INSTANCE.getBank().add(loanAmount);
					Loan.getLoans().remove(loan);
				}
			}
		}
	}

	private boolean hasActiveLoan(User loaner) {
		Iterator<Loan> iterator = Loan.getLoans().iterator();
		while (iterator.hasNext()) {
			Loan loan = iterator.next();
			if (loan.getLoaner().equals(loaner)) {
				return true;
			}
		}
		return false;
	}

	public Money getBank() {
		return bank;
	}
}
