package controller.bank;

import java.util.Iterator;

import model.User;
import server.ActiveSessions;
import server.ClientSession;

public enum BankCollector {
	INSTANCE;
	void punishLoaner(User loaner) {
		Iterator<ClientSession> iterator = ActiveSessions.iterator();
		while (iterator.hasNext()) {
			ClientSession clientSession = iterator.next();
			if (clientSession.getClientUser().equals(loaner)) {
				clientSession.getOutboundMessages().addMessage(
						loaner.getName()
								+ " was thrown in the Baltic Sea for failing to pay back loans");
				clientSession.stopExecuting();
				User.getUsers().remove(loaner);
				break;
			}
		}
	}

}
