package controller;

import java.io.PrintWriter;
import java.util.Iterator;

import model.Game;
import model.Money;
import model.Team;
import model.User;
import presenter.Presenter;
import server.ActiveSessions;
import server.ClientSession;
import server.OutboundMessages;
import util.LoggerAndNetWriter;
import util.Util;
import controller.applicationlogic.BetMachine;
import controller.applicationlogic.GameSabotager;
import controller.applicationlogic.TeamInvestor;
import controller.bank.Bank;
import controller.market.ItemSeller;

/**
 * Singleton that handles input from users.<br>
 * Parses for commands and if finds none, sends sends the input as a message for
 * every user.
 * 
 */
public enum Controller {
	INSTANCE;

	public void manage(OutboundMessages messageToEveryone, User currentUser, String command,
			PrintWriter printWriter) {
		LoggerAndNetWriter userConsoleAndGameLogOutput = new LoggerAndNetWriter(printWriter,
				currentUser);

		command = parseCommand(command);

		String[] arguments = command.split(" ");

		if (usersCmd(arguments)) {
			Iterator<User> users = User.getUsersIterator();
			Presenter.INSTANCE.showUsers(currentUser, userConsoleAndGameLogOutput, users);
			return;
		} else if (moneyCmd(arguments)) {
			Money money = currentUser.getMoney();
			Presenter.INSTANCE.showUserMoney(currentUser, userConsoleAndGameLogOutput, money);
			return;
		} else if (helpCmd(arguments)) {
			Presenter.INSTANCE.showHelp(userConsoleAndGameLogOutput);
			return;
		} else if (gamesCmd(arguments)) {
			Game game = AutomaticGameDispatcher.INSTANCE.getCurrentGame();
			Presenter.INSTANCE.showCurrentGame(currentUser, userConsoleAndGameLogOutput, game);
			return;
		} else if (investCmd(arguments)) {
			Team team = Team.getTeamByName(arguments[1]);
			Money money = new Money(Integer.parseInt(arguments[2]));
			boolean isInvested = TeamInvestor.INSTANCE.investIntoTeam(currentUser, team, money);
			Presenter.INSTANCE.showInvestmentMessage(currentUser, userConsoleAndGameLogOutput,
					isInvested);
			return;
		} else if (sabotageCmd(arguments)) {
			Team team = Team.getTeamByName(arguments[1]);
			Money money = new Money(Integer.parseInt(arguments[2]));
			Game currentGame = AutomaticGameDispatcher.INSTANCE.getCurrentGame();
			boolean isSabotaged = GameSabotager.INSTANCE.sabotageTeamInGame(currentUser, team,
					currentGame, money);
			Presenter.INSTANCE.showSabotageMessage(currentUser, userConsoleAndGameLogOutput,
					isSabotaged);
			return;
		} else if (loanCmd(arguments)) {
			Money money = new Money(Integer.parseInt(arguments[1]));
			boolean isLoanTaken = Bank.INSTANCE.takeLoan(money, currentUser);
			Presenter.INSTANCE.takeLoan(currentUser, userConsoleAndGameLogOutput, isLoanTaken);
			return;
		} else if (betCmd(arguments)) {
			Game game = AutomaticGameDispatcher.INSTANCE.getCurrentGame();
			Team team = Team.getTeamByName(arguments[1]);
			Money money = new Money(Integer.parseInt(arguments[2]));
			boolean isMoneyPut = BetMachine.INSTANCE.putMoneyOn(game, team, money, currentUser);
			Presenter.INSTANCE.createBet(currentUser, userConsoleAndGameLogOutput, isMoneyPut);
			return;
		} else if (buyCmd(arguments)) {
			boolean isItemBought = ItemSeller.INSTANCE.buyItem(arguments[1], currentUser);
			Presenter.INSTANCE.buyItem(currentUser, userConsoleAndGameLogOutput, isItemBought);
			return;
		} else if (useCmd(arguments)) {
			messageToEveryone.addMessage(currentUser.getItem().use());
			return;
		} else if (arguments[0].equals("-quit")) {
			User.getUsers().remove(currentUser);
			Iterator<ClientSession> iterator = ActiveSessions.iterator();
			messageToEveryone.addMessage(currentUser.getName()
					+ " quit gambling");
			while (iterator.hasNext()) {
				ClientSession clientSession = iterator.next();
				if (printWriter.equals(clientSession.getPersonalClientOutput())) {
					clientSession.stopExecuting();
				}
			}

		} else {
			if (!checkIfCommand(command)) {
				messageToEveryone.addMessage(currentUser.getName() + ": " + command);
			} else {
				userConsoleAndGameLogOutput.println("Type -help for a list of commands");
			}
		}
	}

	private boolean useCmd(String[] arguments) {
		return arguments.length == 1 && arguments[0].equals("-use");
	}

	private boolean sabotageCmd(String[] arguments) {
		return arguments.length == 3 && arguments[0].equals("-sabotage")
				&& Util.isInteger(arguments[2]);
	}

	private boolean helpCmd(String[] arguments) {
		return arguments[0].equals("-help");
	}

	private boolean gamesCmd(String[] arguments) {
		return arguments[0].equals("-games");
	}

	private boolean betCmd(String[] arguments) {
		return arguments.length == 3 && arguments[0].equals("-bet") && Util.isInteger(arguments[2]);
	}
	
	private boolean buyCmd(String[] arguments) {
		return arguments.length == 2 && arguments[0].equals("-buy");
	}

	private boolean loanCmd(String[] arguments) {
		return arguments.length == 2 && arguments[0].equals("-loan")
				&& Util.isInteger(arguments[1]);
	}

	private boolean investCmd(String[] arguments) {
		return arguments.length == 3 && arguments[0].equals("-invest")
				&& Util.isInteger(arguments[2]);
	}

	private boolean moneyCmd(String[] arguments) {
		return arguments[0].equals("-money");
	}

	private boolean usersCmd(String[] arguments) {
		return arguments[0].equals("-users");
	}

	private String parseCommand(String command) {
		command = command.trim().replaceAll(" +", " ");
		return command;
	}

	private boolean checkIfCommand(String command) {
		if (command.startsWith("-")) {
			return true;
		}
		return false;
	}

}
