package controller;

import model.Money;
import model.User;

public enum Moneys {
	INSTANCE;
	public boolean checkIfUserHasAmountOfMoney(Money amountOfMoney, User userWhoBets) {
		if (userWhoBets.getMoney().toInt() < amountOfMoney.toInt()) {
			return false;
		}
		return true;
	}

	public boolean subtractMoneyFromUser(User user, Money money) {
		user.getMoney().subtract(money.toInt());
		return true;
	}
}
