package controller.market;

import model.marketitems.CheapVuvuzela;
import model.marketitems.Drums;
import model.marketitems.FancyVuvuzela;
import model.marketitems.Item;

public enum ItemFactory {
	INSTANCE;
	public Item createItem(String itemName) {
		Item item;
		switch (itemName) {
		case "cheapvuvu":
			item = new CheapVuvuzela();
			break;
		case "fancyvuvu":
			item = new FancyVuvuzela();
			break;
		case "drums":
			item = new Drums();
			break;
		default:
			item = null;
		}
		return item;
	}

}
