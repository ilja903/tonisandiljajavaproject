package controller.market;

import util.ErrorInformer;
import model.User;
import model.marketitems.CheapVuvuzela;
import model.marketitems.Drums;
import model.marketitems.FancyVuvuzela;
import model.marketitems.Item;

public enum ItemSeller {
	INSTANCE;

	public boolean buyItem(String itemName, User buyer) {

		if (itemName == null || buyer == null) {
			return false;
		}

		Item item = ItemFactory.INSTANCE.createItem(itemName);

		if (item == null) {
			ErrorInformer.INSTANCE.inform("No such item (see -help for a list)", buyer);
			return false;
		}

		if (item.getPrice() > buyer.getMoney().toInt()) {
			ErrorInformer.INSTANCE.inform("That item is too expensive " + "(" + item.getPrice()
					+ " $)", buyer);
			return false;
		}

		int price = item.getPrice();
		buyer.getMoney().subtract(price);
		buyer.setItem(item);
		return true;

	}
}
