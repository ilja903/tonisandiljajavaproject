package controller;

import java.util.ArrayList;
import java.util.List;

import model.Game;
import model.Team;
import util.LoggerAndNetWriter;
import util.Util;
import controller.applicationlogic.GamePlayer;

/**
 * @author ilja Super thing - creates games on server
 */
public enum AutomaticGameDispatcher {
	INSTANCE;
	private static List<Team> teams = new ArrayList<Team>();
	private static Game currentGame;
	private static int timeToWaitBetweenEachGame = 22000; // 15000 = 15 seconds

	static {
		teams.add(new Team("Madrid"));
		teams.add(new Team("Real"));
		teams.add(new Team("Barcelona"));
		teams.add(new Team("Bayern"));
		generateGame();
	}

	public void executeAutomaticDispacher() {
		new java.util.Timer().schedule(new java.util.TimerTask() {
			@Override
			public void run() {
				endGame();

				LoggerAndNetWriter.sayToEveryOne("HOT NEWS: " + currentGame.getWinningTeam()
						+ " won the game!");

				generateGame();
				executeAutomaticDispacher();
			}
		}, timeToWaitBetweenEachGame);
	}

	public static void generateGame() {
		int totalNumberOfTeams = teams.size();
		Team attackingTeam = teams.get(Util.INSTANCE.generateFrom0ToNminusOne(totalNumberOfTeams));
		Team defendingTeam = teams.get(Util.INSTANCE.generateFrom0ToNminusOne(totalNumberOfTeams));
		while (attackingTeam == defendingTeam) {
			attackingTeam = teams.get(Util.INSTANCE.generateFrom0ToNminusOne(totalNumberOfTeams));
			defendingTeam = teams.get(Util.INSTANCE.generateFrom0ToNminusOne(totalNumberOfTeams));
		}
		currentGame = new Game("Tournament", attackingTeam, defendingTeam);
	}

	public void endGame() {
		GamePlayer.INSTANCE.playGame(currentGame);
	}

	public List<Team> getTeams() {
		return teams;
	}

	public Game getCurrentGame() {
		return currentGame;
	}

	public int getTimeToWaitBetweenEachGame() {
		return timeToWaitBetweenEachGame;
	}
}
