package controller.applicationlogic;

import java.util.Iterator;

import model.Bet;
import model.Game;
import model.Money;
import model.Team;
import model.User;
import util.ErrorInformer;
import controller.Moneys;
import controller.bank.Bank;

/**
 * @author ilja This singleton tkaes bets from players and shares money they won
 */
public enum BetMachine {
	INSTANCE;
	/**
	 * If you won how much money you will get back?
	 */
	private int coefficentOfWining = 2;
	private int bankCutPercentage = 5;

	public boolean putMoneyOn(Game game, Team team, Money money, User userWhoBets) {
		if (game == null || team == null || money == null || userWhoBets == null) {
			ErrorInformer.INSTANCE.inform("Null fields present", userWhoBets);
			return false;
		}

		if (!Moneys.INSTANCE.checkIfUserHasAmountOfMoney(money, userWhoBets)) {
			ErrorInformer.INSTANCE.inform("You don't have enough money to bet", userWhoBets);
			return false;
		}

		if (!(game.getAttackingTeam().equals(team) || game.getDefendingTeam().equals(team))) {
			ErrorInformer.INSTANCE.inform("That team is not playing", userWhoBets);
			return false;
		}

		userWhoBets.getMoney().subtract(money.toInt());
		int bankCut = money.toInt() * (bankCutPercentage) / 100;
		Bank.INSTANCE.getBank().add(bankCut);
		Money betMoney = new Money(money.toInt() - bankCut);
		return Bet.getBets().add(new Bet(game, team, betMoney, userWhoBets));

	}

	public boolean update() {
		dispenseWinnings();
		Bank.INSTANCE.manageLoans();
		return true;
	}

	private void dispenseWinnings() {
		Iterator<Bet> iterator = Bet.getBets().iterator();
		while (iterator.hasNext()) {
			Bet bet = iterator.next();
			if (checkIfPlayerWon(bet)) {
				bet.getUserWhoBets().getMoney().add(bet.getMoney().toInt() * coefficentOfWining);
			}
		}
		Bet.getBets().clear();
	}

	private boolean checkIfPlayerWon(Bet bet) {
		if (bet.getTeam().equals(bet.getGame().getWinningTeam())) {
			return true;
		}
		return false;
	}

}
