package controller.applicationlogic;

import java.util.ArrayList;
import java.util.List;

import util.ErrorInformer;

import model.Game;
import model.Money;
import model.Sabotage;
import model.Team;
import model.User;
import controller.Moneys;
import controller.applicationlogic.gameplayinglogic.SabotageGamePlayingLogic;

/**
 * Singleton that manages sabotages in games.
 * Provides methods for sabotaging teams that are playing 
 * and for collecting all sabotages concerning a game.
 *
 */
public enum GameSabotager {
	INSTANCE;

	/**
	 * Tries to sabotage the given team in a given game, decreasing its 
	 * chance to win the game.
	 * @param userWhoSabotages
	 * @param teamToSabotage
	 * @param gameToSabotage
	 * @param moneyForSabotaging
	 * @return false in case the sabotaging User does not have enough money 
	 * and true otherwise
	 */
	public boolean sabotageTeamInGame(User userWhoSabotages, Team teamToSabotage,
			Game gameToSabotage, Money moneyForSabotaging) {
		if (!Moneys.INSTANCE.checkIfUserHasAmountOfMoney(moneyForSabotaging, userWhoSabotages)) {
			ErrorInformer.INSTANCE.inform("You don't have enough money to sabotage", userWhoSabotages);
			return false;
		}
		Moneys.INSTANCE.subtractMoneyFromUser(userWhoSabotages, moneyForSabotaging);
		sabotageGame(teamToSabotage, gameToSabotage, moneyForSabotaging);
		return true;
	}

	/**
	 * Sets the gamePlayingLogic to SabotagePlayingLogic and 
	 * adds the Sabotage to the list containing all sabotages.
	 * @param teamToSabotage
	 * @param gameToSabotage
	 * @param moneyInvestedIntoSabotaging
	 * @return
	 */
	private boolean sabotageGame(Team teamToSabotage, Game gameToSabotage,
			Money moneyInvestedIntoSabotaging) {
		GamePlayer.INSTANCE.setGamePlayingLogic(SabotageGamePlayingLogic.INSTANCE);
		Sabotage.getSabotages().add(
				new Sabotage(teamToSabotage, gameToSabotage, moneyInvestedIntoSabotaging));
		return true;
	}

	/**
	 * Returns a list with all the sabotages in the given game
	 * @param game
	 * @return list of sabotages in the given game
	 */
	public List<Sabotage> collectSabotagesInGivenGame(final Game game) {
		List<Sabotage> sabotagesInGivenGame = new ArrayList<>();
		List<Sabotage> allSabotages = Sabotage.getSabotages();
		for (Sabotage sabotage : allSabotages) {
			if (sabotage.getGameToSabotage().equals(game)) {
				sabotagesInGivenGame.add(sabotage);
			}
		}
		return sabotagesInGivenGame;

	}
}
