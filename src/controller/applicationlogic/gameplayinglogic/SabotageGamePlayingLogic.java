package controller.applicationlogic.gameplayinglogic;

import java.util.List;

import controller.applicationlogic.GameSabotager;
import util.Util;
import model.Game;
import model.Sabotage;
import model.Team;

/**
 * Singleton that contains the logic of determining the winner of 
 * a game based on a combination of a random factor and the 
 * money invested in sabotaging the teams (thus lowering the
 * teams chance to win)
 *
 */
public enum SabotageGamePlayingLogic implements GamePlayingLogic {
	INSTANCE;
	private static int costPerTenPercentsOfMood = 10000;

	@Override
	public Team getWinner(Game game) {

		int attackingTeamMood = 100 + Util.INSTANCE.generateFrom0ToN(50);
		int defendingTeamMood = 100 + Util.INSTANCE.generateFrom0ToN(50);
		System.err.println("atta " +attackingTeamMood);
		System.err.println("def " +defendingTeamMood);
		Team attackingTeam = game.getAttackingTeam();
		Team defendingTeam = game.getDefendingTeam();

		List<Sabotage> sabotagesInGivenGame = GameSabotager.INSTANCE.collectSabotagesInGivenGame(game);
		int moneyInvestedIntoSabotagingAttackingTeam = 0;
		int moneyInvestedIntoSabotagingDefendingTeam = 0;
		
		for (Sabotage sabotage : sabotagesInGivenGame) {
			if (sabotage.getTeamToSabotage().equals(attackingTeam)) {
				moneyInvestedIntoSabotagingAttackingTeam += sabotage.getMoneyInvestedIntoSabotage().toInt();
			}else if(sabotage.getTeamToSabotage().equals(defendingTeam)) {
				moneyInvestedIntoSabotagingDefendingTeam += sabotage.getMoneyInvestedIntoSabotage().toInt();
			}
		}
		
		attackingTeamMood -= (moneyInvestedIntoSabotagingAttackingTeam/costPerTenPercentsOfMood)*10;
		defendingTeamMood -= (moneyInvestedIntoSabotagingDefendingTeam/costPerTenPercentsOfMood)*10;

		if (attackingTeamMood * attackingTeam.getLevel() > defendingTeamMood * defendingTeam.getLevel()) {
			return game.getAttackingTeam();
		} else {
			return game.getDefendingTeam();
		}
	}
}
