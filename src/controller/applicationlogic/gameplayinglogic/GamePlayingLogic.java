package controller.applicationlogic.gameplayinglogic;

import model.Game;
import model.Team;

public interface GamePlayingLogic {

	/**
	 * @param game
	 * @return the winner of the specified game
	 */
	public Team getWinner(Game game);

}
