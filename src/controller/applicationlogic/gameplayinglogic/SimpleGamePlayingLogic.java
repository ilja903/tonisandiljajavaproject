package controller.applicationlogic.gameplayinglogic;

import model.Game;
import model.Team;

/**
 * Singleton that determines a winner of a game purely by their level.
 *
 */
public enum SimpleGamePlayingLogic implements GamePlayingLogic {
	INSTANCE;
	@Override
	public Team getWinner(Game game) {
		if (game.getAttackingTeam().getLevel() > game.getDefendingTeam().getLevel()) {
			return game.getAttackingTeam();
		} else {
			return game.getDefendingTeam();
		}
	}

}
