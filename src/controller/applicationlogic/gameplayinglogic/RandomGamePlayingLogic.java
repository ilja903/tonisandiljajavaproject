package controller.applicationlogic.gameplayinglogic;

import util.Util;
import model.Game;
import model.Team;

/**
 * Singleton that contains the logic for determining the winner of a game randomly.
 *
 */
public enum RandomGamePlayingLogic implements GamePlayingLogic {
	INSTANCE;
	@Override
	public Team getWinner(Game game) {
		int attackingTeamMood = 100 + Util.INSTANCE.generateFrom0ToN(50);
		int defendingTeamMood = 100 + Util.INSTANCE.generateFrom0ToN(50);
		if (attackingTeamMood * game.getAttackingTeam().getLevel() > defendingTeamMood
				* game.getDefendingTeam().getLevel()) {
			return game.getAttackingTeam();
		} else {
			return game.getDefendingTeam();
		}
	}

}
