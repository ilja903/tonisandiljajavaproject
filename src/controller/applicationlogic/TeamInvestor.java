package controller.applicationlogic;

import controller.Moneys;
import model.Money;
import model.Team;
import model.User;

/**
 * Singleton that manages investments in teams, which increase the 
 * teams levels which in turn increase the teams chance of winning 
 * games.
 */
public enum TeamInvestor {
	INSTANCE;
	/**
	 * the cost of a single level
	 */
	private static int oneLevelCost = 10000;

	/**
	 * Tries to invest the given amount in the given team, 
	 * increasing the teams level according to the money 
	 * provided
	 * @param userWhoInvests
	 * @param teamToInvest
	 * @param moneyToInvest
	 * @return false in case the User does not have enough money
	 */
	public boolean investIntoTeam(User userWhoInvests, Team teamToInvest, Money moneyToInvest) {
		if (!Moneys.INSTANCE.checkIfUserHasAmountOfMoney(moneyToInvest, userWhoInvests)) {
			return false;
		}

		int levelsToAdd = moneyToInvest.toInt() / oneLevelCost;

		{
			Moneys.INSTANCE.subtractMoneyFromUser(userWhoInvests, moneyToInvest);
			teamToInvest.incrementLevelNtimes(levelsToAdd);
		}

		return true;
	}
}
