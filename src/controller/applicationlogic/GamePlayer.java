package controller.applicationlogic;

import model.Game;
import model.GameState;
import model.Team;
import controller.applicationlogic.gameplayinglogic.GamePlayingLogic;
import controller.applicationlogic.gameplayinglogic.RandomGamePlayingLogic;
import exceptions.TeamHasAlreadyWonException;

/**
 * @author ilja Plays game you give to him, by using different types of logic
 *         (sabotages and levelups)
 */
public enum GamePlayer {
	INSTANCE;
	GamePlayingLogic gamePlayingLogic = RandomGamePlayingLogic.INSTANCE;

	public void setGamePlayingLogic(GamePlayingLogic gamePlayingLogic) {
		this.gamePlayingLogic = gamePlayingLogic;
	}

	public void playGame(Game game) {
		gamePlayingLogic(game);
	}

	/**
	 * Plays out a game according to the current gamePlayingLogic. 
	 * Increments the winning teams level and notifies BetMachine 
	 * to handle all the activities related to the game (bets, loans)
	 * @param game - game to play
	 */
	private void gamePlayingLogic(Game game) {
		Team winner = getWinner(game);
		winner.incrementLevel();
		setGameWinner(game, winner);
		game.setGameState(GameState.FINISHED);
		notifyBetMachine();
	}

	/**
	 * Determines the winner of the game according 
	 * to the current gamePlayingLogic
	 * @param game
	 * @return the winning team
	 */
	private Team getWinner(Game game) {
		return gamePlayingLogic.getWinner(game);
	}

	/**
	 * Sets the games winning team to be the specified winnerTeam
	 * @param game
	 * @param winnerTeam
	 * @return
	 */
	private boolean setGameWinner(Game game, Team winnerTeam) {
		try {
			game.setWinningTeam(winnerTeam);
			return true;
		} catch (TeamHasAlreadyWonException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Notifies BetMachine to handle all the 
	 * activities related to the game (bets, loans)
	 */
	private boolean notifyBetMachine() {
		return BetMachine.INSTANCE.update();
	}

}
