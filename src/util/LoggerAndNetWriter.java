package util;

import java.io.PrintWriter;

import model.User;
import server.ClientSession;

/**
 * @author ilja
 * writes output to user and mirrors it to log
 *
 */
public class LoggerAndNetWriter {
	private final PrintWriter personalClientOutput;
	private final String userName;

	public LoggerAndNetWriter(PrintWriter personalClientOutput, User user) {
		this.personalClientOutput = personalClientOutput;
		this.userName = user.getName();
	}

	public void println(Object x) {
		GameLogger.write(userName + ":::  " + x);
		personalClientOutput.println(x);
	}

	public void println() {
		personalClientOutput.println();
	}

	public void printf(String message) {
		personalClientOutput.printf(message, null);
	}

	public static void sayToEveryOne(String message) {
		/* Dont use it like that too much */
		ClientSession.messageToEveryone.addMessage(message);
		GameLogger.write(message);
	}

}