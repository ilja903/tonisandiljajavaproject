package util;

import java.util.Random;

/**
 * Singleton that provides some utility methods, such as
 * generating random numbers and checking if a String 
 * represents an Integer.
 *
 */
public enum Util {
	INSTANCE;

	private Random rand = new Random();

	/**
	 * generates from 0 to N-1 -_ this means that if N is 9 maximum generated
	 * value will be 8
	 * 
	 * @param N
	 * @return
	 */
	public int generateFrom0ToNminusOne(int N) {
		return rand.nextInt(N);
	}
	
	public int generateFrom0ToN(int N) {
		return rand.nextInt(1+N);
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
	
}
