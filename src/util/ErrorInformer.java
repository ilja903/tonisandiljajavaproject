package util;

import java.util.Iterator;

import server.ActiveSessions;
import server.ClientSession;
import model.User;

public enum ErrorInformer {
	INSTANCE;
	public void inform(String message, User user) {
		Iterator<ClientSession> iterator = ActiveSessions.iterator();
		while (iterator.hasNext()) {
			ClientSession session = iterator.next();
			User clientUser = session.getClientUser();
			if (clientUser == user) {
				session.getPersonalClientOutput().printf("%n" + message + "%n");
			}
		}
	}
}
