package util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class GameLogger {
	private final static Logger log = Logger.getLogger(GameLogger.class.getName());

	static {
		FileHandler hand;
		try {
			hand = new FileHandler("logs/GameLog.log", false );
			log.addHandler(hand);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void write(Object object) {
		log.info(object.toString());
	}
	
	
}
