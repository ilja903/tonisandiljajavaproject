package model;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import model.marketitems.Item;

import server.OutboundMessages;

/**
 * An user that can place bets on games, sabotage teams in games, 
 * invest in teams, take loans. Has a certain amount of money to do so.
 */
public class User {
	private static List<User> users = new CopyOnWriteArrayList<User>();

	private String name;
	private Money money;
	private Item item;
	PrintWriter clientChannel;
	OutboundMessages publicChannel;

	public User(String name) {
		this.name = name;
		this.money = new Money(1000000);
		this.item = new model.marketitems.CheapVuvuzela();
		users.add(this);
	}
	
	public void setItem(Item item) {
		this.item = item;
	}
	
	public Item getItem() {
		return item;
	}

	public String getName() {
		return name;
	}

	public Money getMoney() {
		return money;
	}

	public static List<User> getUsers() {
		return users;
	}

	public static Iterator<User> getUsersIterator() {
		return users.iterator();
	}

	public static User getUserByName(String name) {
		Iterator<User> iterator = getUsersIterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.getName().equals(name)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "User " + name + ", who has " + money;
	}

}
