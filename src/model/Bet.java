package model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A bet about the outcome of a game that specifies 
 * the team to win and the money offered.
 */
public class Bet {
	/**
	 * List of all active bets.
	 */
	private static List<Bet> bets = new CopyOnWriteArrayList<Bet>();
	private final Game game;
	private final Team team;
	private Money money;
	private final User userWhoBets;

	public Bet(Game game, Team team, Money money, User userWhoBets) {
		this.game = game;
		this.team = team;
		this.money = money;
		this.userWhoBets = userWhoBets;
	}
	
	public void setMoney(Money money) {
		this.money = money;
	}

	public Game getGame() {
		return game;
	}

	public Team getTeam() {
		return team;
	}

	public Money getMoney() {
		return money;
	}

	public User getUserWhoBets() {
		return userWhoBets;
	}

	public static List<Bet> getBets() {
		return bets;
	}

	@Override
	public String toString() {
		return "Bet [game=" + game + ", team=" + team + ", money=" + money + ", userWhoBets="
				+ userWhoBets + "]";
	}

}
