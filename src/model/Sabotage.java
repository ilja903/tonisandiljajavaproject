package model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A sabotage represents a certain amount of money someone has invested 
 * to reduce a teams chance of winning in a certain game. <br>
 * Also contains the list of all sabotages.
 *
 */
public class Sabotage {
	private static List<Sabotage> sabotages = new CopyOnWriteArrayList<Sabotage>();
	private Team teamToSabotage;
	private Game gameToSabotage;
	private Money moneyInvestedIntoSabotage;

	public Sabotage(Team teamToSabotage, Game gameToSabotage, Money moneyInvestedIntoSabotaging) {
		this.teamToSabotage = teamToSabotage;
		this.gameToSabotage = gameToSabotage;
		this.moneyInvestedIntoSabotage = moneyInvestedIntoSabotaging;
	}

	public Game getGameToSabotage() {
		return gameToSabotage;
	}

	public Team getTeamToSabotage() {
		return teamToSabotage;
	}

	public Money getMoneyInvestedIntoSabotage() {
		return moneyInvestedIntoSabotage;
	}

	public static List<Sabotage> getSabotages() {
		return sabotages;

	}

	@Override
	public String toString() {
		return "Sabotage [teamToSabotage=" + teamToSabotage + ", gameToSabotage=" + gameToSabotage
				+ ", moneyInvestedIntoSabotage=" + moneyInvestedIntoSabotage + "]";
	}

}
