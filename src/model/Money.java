package model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * An object containing an amount of currency that 
 * is usable in the game.
 *
 */
public class Money {
	private AtomicInteger amount;

	public Money(int amount) {
		this.amount = new AtomicInteger(amount);
	}

	/**
	 * @return the amount of currency as an integer that the
	 * Money currently holds
	 */
	public int toInt() {
		return amount.get();
	}

	public void set(int newAmount) {
		amount.set(newAmount);
	}

	public void add(int amountToAdd) {
		amount.set(amount.get() + amountToAdd);
	}

	public void subtract(int amountToSubtract) {
		amount.set(amount.get() - amountToSubtract);
	}

	@Override
	public String toString() {
		return toInt() + " $$$";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		return true;
	}

}
