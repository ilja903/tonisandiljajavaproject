package model;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A team that can take part in Games. Has a level, which being 
 * higher gives a higher chance to win a Game.
 *
 */
public class Team {
	private static List<Team> teams = new CopyOnWriteArrayList<Team>();
	private String name;
	private AtomicInteger level = new AtomicInteger(1);

	public Team(String name) {
		this.name = name;
		teams.add(this);
	}

	public void incrementLevel() {
		level.incrementAndGet();
	}

	public void incrementLevelNtimes(int levelsToAdd) {
		level.addAndGet(levelsToAdd);
	}

	public String getName() {
		return name;
	}

	public int getLevel() {
		return level.get();
	}

	public static Iterator<Team> getUsersIterator() {
		return teams.iterator();
	}

	public static List<Team> getTeams() {
		return teams;
	}

	public static Team getTeamByName(String name) {
		for (Team team : teams) {
			if (team.getName().equalsIgnoreCase(name)) {
				System.out.println(team);
				return team;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "Team " + name;
	}

}
