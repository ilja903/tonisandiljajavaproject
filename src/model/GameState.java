package model;

/**
 * An enumeration containing the possible game states 
 * that a game can be in. <br>
 * * WAITING - the game has not yet finished <br>
 * * FINISHED - the game has ended
 */
public enum GameState {
	WAITING, FINISHED;
}
