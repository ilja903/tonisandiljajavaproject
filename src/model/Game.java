package model;

import controller.AutomaticGameDispatcher;
import exceptions.TeamHasAlreadyWonException;

/**
 * A game between two teams.
 * 
 */
public class Game {

	private String name;
	final private Team attackingTeam;
	final private Team defendingTeam;
	private Team winningTeam;
	private GameState gameState = GameState.WAITING;
	private long creatementTime = System.currentTimeMillis();

	public Game(String name, Team attackingTeam, Team defendingTeam) {
		this.name = name;
		this.attackingTeam = attackingTeam;
		this.defendingTeam = defendingTeam;
	}

	public void setGameState(final GameState gameState) {
		this.gameState = gameState;
	}

	/**
	 * Sets a team as the winner for the game.
	 * 
	 * @param winningTeam
	 * @throws TeamHasAlreadyWonException
	 *             in case a winning Team has already been set for the game
	 */
	public void setWinningTeam(final Team winningTeam) throws TeamHasAlreadyWonException {
		if (this.winningTeam != null) {
			throw new TeamHasAlreadyWonException("team has already won!");
		} else {
			this.winningTeam = winningTeam;
			this.gameState = GameState.FINISHED;
		}
	}

	public String getName() {
		return name;
	}

	public GameState getGameState() {
		return gameState;
	}

	public Team getAttackingTeam() {
		return attackingTeam;
	}

	public Team getDefendingTeam() {
		return defendingTeam;
	}

	public Team getWinningTeam() {
		return winningTeam;
	}

	/**
	 * @return time for the game to end in seconds
	 */
	public long getSecondsTOGameEnd() {
		return (creatementTime + AutomaticGameDispatcher.INSTANCE.
						getTimeToWaitBetweenEachGame() - System.currentTimeMillis())/1000;
	}

	@Override
	public String toString() {
		return name + " between " + attackingTeam + " and " + defendingTeam + " is "
				+ getGameState() + " and will start in " + getSecondsTOGameEnd() + " seconds";
	}

}
