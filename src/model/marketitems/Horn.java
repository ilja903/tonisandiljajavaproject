package model.marketitems;

public abstract class Horn extends Item {
	
	@Override
	public String use() {
		return blow();
	}
	
	public abstract String blow();
	
}
