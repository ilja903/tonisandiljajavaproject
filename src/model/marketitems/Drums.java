package model.marketitems;


public class Drums extends Item {
	
	static int price = 500000;
	static String name = "drums";

	@Override
	public String use() {
		return "pumm-pumm-PUMM--pumm-pumm-PUMM";
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}

}
