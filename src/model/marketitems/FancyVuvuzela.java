package model.marketitems;


public class FancyVuvuzela extends Horn {
	
	static int price = 1000000;
	static String name = "fancyvuvu";

	@Override
	public String blow() {
		return "PRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP";
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}
	
}
