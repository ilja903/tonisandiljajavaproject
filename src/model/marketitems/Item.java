package model.marketitems;

public abstract class Item {
	int price;
	String name;
	public abstract String use();
	public abstract int getPrice();
	public abstract String getName();
}
