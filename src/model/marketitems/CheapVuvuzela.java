package model.marketitems;


public class CheapVuvuzela extends Horn {
	
	static int price = 15000;
	static String name = "cheapvuvu";

	@Override
	public String blow() {
		return "PRAAP";
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}

}
