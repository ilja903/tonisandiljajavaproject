package model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A loan by a User for a certain amount of money. 
 * Needs to be payed back with a coefficent added 
 * after a certain amount of games have passed. 
 * If the User fails to do so, elimination from the 
 * Game will occur.
 *
 */
public class Loan {

	private final User loaner;
	private Money money;
	private int payBackCounter;
	
	private static final int TIME_TO_PAY = 1;
	
	private static List<Loan> loans = new CopyOnWriteArrayList<Loan>();

	public static List<Loan> getLoans() {
		return loans;
	}

	public Loan(Money money, User loaner) {
		this.loaner = loaner;
		this.setMoney(money);
		this.payBackCounter = TIME_TO_PAY;
	}

	public User getLoaner() {
		return loaner;
	}

	public Money getMoney() {
		return money;
	}

	public void setMoney(Money money) {
		this.money = money;
	}
	
	public void decreasePayBackCounter() {
		payBackCounter--;
	}

	public int getPayBackCounter() {
		return payBackCounter;
	}
}
